Twitter Histogram Client.

Provides a histogram chart to show when twitter accounts posts their status updates.
Collects a max of 500 of the latest tweets by twitter account.

DEMO

http://ec2-52-62-126-116.ap-southeast-2.compute.amazonaws.com:80

Install dependencies using npm install

** If running at a new ip address, the api client will need updating with a new CORS option
** to allow the ajax calls.

To start server run: npm start
goto localhost:3002 to see application in action

To start dev server including hot-loader run
npm start
npm start webpack-dev-server
goto localhost:3000 to see dev server

Tests to demonstrate basic functionality can be found in /test folder and run through PHPUnit

TODO:
 - add production config to webpack to strip and minify production assets (current the AWS instance is running the dev environment)
 - add a loading page for when inital js file is downloading
 - add a config settings files / environmental settings to reduce dependancy on hard coded server variables
 - add additional test-coverage, only bare minimum present.
