var MainComponent = require('./components/mainComponent');
var React = require('react');
var ReactDOM = require('react-dom');
require('./assets/scss/style.scss');

// Load the react component into the base element
ReactDOM.render(
    <MainComponent />,
    document.getElementById('react')
);