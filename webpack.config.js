var webpack = require('webpack');
var path = require('path');

var webpackConfig = {
    resolve: {
        extensions: ['', '.js', '.jsx']
    },
    entry: [
        'webpack-dev-server/client?http://localhost:3000',
        'webpack/hot/only-dev-server',
        './app.js'
    ],
    output: {
        path: path.resolve('./build/js'),
        publicPath: '/js/',
        filename: 'main.js'
    },
    module: {
        loaders: [
            {
                test: /style.scss$/,
                loader: ('style-loader!css-loader!sass-loader?outputStyle=expanded')
            },
            {
                test: /\.js$/,
                loader: 'babel',
                query: {
                    presets: ['react']
                }
            },
            {
                test: /\.(js|jsx)$/,
                exclude: /node_modules/,
                loader: require.resolve('react-hot-loader')
            },
            {
                test: /\.(png|jpg|jpeg|gif)$/,
                loader: "url-loader?limit=50000000"
            },
        ]
    },
    plugins: [
        new webpack.HotModuleReplacementPlugin(),
        new webpack.NoErrorsPlugin(),
        new webpack.DefinePlugin({
            'process.env': {
                NODE_ENV: JSON.stringify('development')
            }
        })
    ]
};

module.exports = webpackConfig;
