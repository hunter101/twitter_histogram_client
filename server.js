var http = require('http');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var fs = require('fs');
var express = require('express');
var app = express();

app.use(bodyParser.json());       // to support JSON-encoded bodies
app.use(bodyParser.urlencoded({     // to support URL-encoded bodies
    extended: true
}));
app.use(cookieParser());

app.use("/", express.static(__dirname + '/build'));

app.get('/', function(req, res){
    res.sendFile(__dirname + '/index.html');
});

app.listen(3002, function () {
    console.log("Server listening on port 3002");
});