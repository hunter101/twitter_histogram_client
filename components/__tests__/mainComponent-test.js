var React = require('react'),
    TestUtils = require('react-addons-test-utils'),
    MainComponent = require('./../MainComponent'),
    expect = require('expect');

describe('MainComponent', function () {

    it("Renders without errors", function () {
        var root = TestUtils.renderIntoDocument(<MainComponent />);
        expect(root).toExist();
    });

});
