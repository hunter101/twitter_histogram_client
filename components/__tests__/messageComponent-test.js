var React = require('react'),
    TestUtils = require('react-addons-test-utils'),
    MessageComponent = require('./../messageComponent'),
    expect = require('expect');

describe('MessageComponent', function () {

    var message = {
        status: "error",
        text: "This is a sample message"
    };


    it("renders a message", function () {

        var component = TestUtils.renderIntoDocument(
            <MessageComponent message={message} />
        );

        var p = TestUtils.findRenderedDOMComponentWithTag(
            component, 'p'
        );

        expect(p.textContent).toEqual(message.text);
    });

    it("Creates the correct class based on the message status", function () {
        var component = TestUtils.renderIntoDocument(
            <MessageComponent message={message} />
        );

        var found = TestUtils.findRenderedDOMComponentWithClass(component, 'error');
        expect(found).toExist();
    })
});
