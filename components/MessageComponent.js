var React = require('react');

var MessageComponent = React.createClass({

    render: function () {

        var messageText = this.props.message.text;
        var messageClass = ["callout", this.props.message.status];

        return (
            <div className={messageClass.join(" ")}>
                <p>{ messageText }</p>
            </div>
        )
    }
});

module.exports = MessageComponent;