var React = require('react');
var MessageComponent = require('./MessageComponent');
var superagent = require('superagent');
var _ = require('underscore');
var BarChart = require('react-d3').BarChart;

var MainComponent = React.createClass({

    apiUrl: "http://ec2-52-62-126-116.ap-southeast-2.compute.amazonaws.com:8080",

    getInitialState: function () {
        return {
            username: "hardtofind_",
            totalPostsCount: null,
            message: {
                status: "notice",
                text: "Enter a username and hit submit to see their favourite hours of the day to post self-important status updates..."
            },
            graphWidth: 400,
            chartData: null,
            containerPosition: "middle"
        }
    },

    componentDidMount: function () {

        // Set the graph to be 100% width of main container
        // SVG isn't resizable to responsive devices so width
        // needs to be set manually to ensure fits to bounding device.
        var width = this.refs.main.offsetWidth;
        this.setState({
            graphWidth: width
        })
    },

    handleChange: function (event) {
        if (event.key === "Enter") {
            this.handleSubmit(event);
        } else {
            this.setState({username: event.target.value})
        }
    },

    handleSubmit: function (event) {
        event.preventDefault();
        var username = this.state.username;

        // Set a nice load state so the user knows they
        // are waiting for info to be returned.
        this.setState({
            message: {
                status: "secondary",
                text: "Loading..."
            },
            containerPosition: "top",
            chartData: null
        });

        // Query the application api to retrieve recent twitter status updates.
        superagent
            .get(this.apiUrl + '/api/v1.0/twitter/latest_posts/' + username)
            .set('Accept', 'application/json')
            .withCredentials()
            .end((err, res) => {

                // Catch any errors thrown up by superagent
                if (err) {
                    this.setState({
                        message: {
                            status: "alert",
                            text: "An error occured fetching the data"
                        },
                    });
                    console.log(err);
                } else {
                    var json = JSON.parse(res.text);

                    // Catch any errors thrown by the application
                    if (json.status === "error") {
                        this.setState({
                            message: {
                                status: "warning",
                                text: json.message
                            }
                        });
                        return;
                    }

                    // Array of status updates.
                    var payload = JSON.parse(json.data);

                    // Group the returned twitter posts by the
                    // hour they were made and count them.
                    var totals = _.countBy(payload, function (item) {
                        return new Date(item.created_at).getHours();
                    });

                    // Get the total number of posts returned from the query
                    // ...Yes, we could just get the payload length, but
                    // practising functional programming is fun.
                    var totalPostsCount = _.reduce(totals, function (memo, num) {
                        return memo + num
                    }, 0);

                    // Create an array of values that can be used by D3
                    // https://reactiva.github.io/react-d3-website/.
                    var values = _.map(Array(24), function (item, k) {
                        return {"x": k, "y": totals[k] || 0};
                    });

                    this.setState({
                        chartData: [{
                            name: 'series1',
                            values: values
                        }],
                        totalPostsCount: totalPostsCount
                    })
                }
            })
    },

    render: function () {

        var username = this.state.username;
        var chartData = this.state.chartData;
        var width = this.state.graphWidth;
        var message = this.state.message;
        var totalPostsCount = this.state.totalPostsCount;
        var containerPosition = this.state.containerPosition;
        var containerClass = ['container', 'row', containerPosition];

        return (
            <div className={containerClass.join(" ")}>
                <div className="small-12 large-6 small-centered columns" ref="main">
                    <h1>Twitter Histogram</h1>

                    <form onSubmit={this.handleSubmit}>
                        <input placeholder="Enter twitter username" type="text" value={username}
                               onChange={this.handleChange}/>
                        <button type="submit" className="button">Go get me some</button>
                    </form>
                    <div className="results-area">
                        { chartData ?
                            <BarChart
                                data={chartData}
                                width={width}
                                height={300}
                                rangeRoundBandsPadding={0.1}
                                fill={'#3182bd'}
                                title={'Total Posts:' + totalPostsCount}
                                yAxisLabel='Number of Posts'
                                xAxisLabel='Hour of Day'
                                />
                            : <MessageComponent message={message}/> }
                    </div>
                </div>
            </div>
        )
    }
});

module.exports = MainComponent;
